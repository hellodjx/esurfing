# 广东天翼校园 Python 客户端
仅在五邑大学进行过测试，感谢 https://github.com/6DDUU6/SchoolAuthentication 。

## 使用方法
先在网页端进行上线操作，然后把 config.template.ini 重命名为 config.ini，填入合适的配置。运行 pip3 install requests 安装依赖，执行 python3 main.py 使用本应用。

## 致谢
感谢旭华同学提交的 [MR](https://gitlab.com/hellodjx/esurfing/-/merge_requests/2)，实现了自动获取 IP 地址和 MAC 地址。
