import requests
import configparser
import time
import datetime
import hashlib
import socket
import uuid
from collections import namedtuple

CONFIG_PATH = "./config.ini"
SECERT = "Eshore!@#"
ISWIFI = "4060"
SESSION = requests.session()
SESSION.headers[
    "User-Agent"
] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36"
NASIP = "119.146.175.80"


Config = namedtuple(
    "Config", ["account", "password", "local_ip", "mac_address"]
)


def get_ip_address() -> str:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]

    ip = str(ip).split("\n")[0]
    return ip


def get_mac_address() -> str:
    mac = uuid.UUID(int=uuid.getnode()).hex[-12:]
    ic = ":".join([mac[e: e + 2] for e in range(0, 11, 2)])

    ic = str(ic).split("\n")[0]
    ic = ic.replace(":", "-")
    return ic.upper()


def print_error_and_exit(data: str):
    print(f"ERROR: {data}")
    _ = input("按回车键退出")
    exit(-1)


def read_config(path: str) -> Config:
    config = configparser.ConfigParser()
    config.read(path)
    ip = get_ip_address()
    mac = get_mac_address()
    config_tuple = Config(
        config["record"]["account"], config["record"]["password"], ip, mac
    )

    return config_tuple


def get_md5_str(s: str) -> str:
    return hashlib.md5(s.encode("utf-8")).hexdigest().upper()


def get_verify_code(config: Config) -> str:
    url = "http://enet.10000.gd.cn:10001/client/challenge"
    time_stamp = str(time.time()).replace(".", "")[0:13]
    md5_string = get_md5_str(
        config.local_ip + NASIP + config.mac_address + time_stamp + SECERT
    )
    data = {
        "username": config.account,
        "clientip": config.local_ip,
        "nasip": NASIP,
        "mac": config.mac_address,
        "timestamp": time_stamp,
        "authenticator": md5_string,
    }
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
        "Content-Type": "application/json",
        "Accept": "*/*",
        "Host": "enet.10000.gd.cn:10001",
    }
    response = SESSION.post(url, json=data, headers=headers, timeout=3)
    verify_code = response.json()["challenge"]
    return verify_code


def login(verify_code: str) -> str:
    url = "http://enet.10000.gd.cn:10001/client/login"
    time_stamp = str(time.time()).replace(".", "")[0:13]
    md5_string = get_md5_str(
        config.local_ip
        + NASIP
        + config.mac_address
        + time_stamp
        + verify_code
        + SECERT
    )
    data = {
        "username": config.account,
        "password": config.password,
        "clientip": config.local_ip,
        "nasip": NASIP,
        "mac": config.mac_address,
        "iswifi": ISWIFI,
        "timestamp": time_stamp,
        "authenticator": md5_string,
    }
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
        "Content-Type": "application/json",
        "Accept": "*/*",
        "Host": "enet.10000.gd.cn:10001",
    }
    response = SESSION.post(url, json=data, headers=headers, timeout=3)

    return response.text


def keep_active():
    url = "http://enet.10000.gd.cn:8001/hbservice/client/active"
    time_stamp = str(time.time()).replace(".", "")[0:13]
    md5_string = get_md5_str(
        config.local_ip + NASIP + config.mac_address + time_stamp + SECERT
    )
    payload = {
        "username": config.account,
        "clientip": config.local_ip,
        "nasip": NASIP,
        "mac": config.mac_address,
        "timestamp": time_stamp,
        "authenticator": md5_string,
    }
    SESSION.get(url, params=payload, timeout=3)


if __name__ == "__main__":
    config = read_config(CONFIG_PATH)
    print(f"本地IP为：{config.local_ip}")
    print(f"本地MAC地址为：{config.mac_address}")

    try:
        verify_code = get_verify_code(config)
    except Exception as e:
        print_error_and_exit(e)
    print(f"获取验证码为：{verify_code}")
    try:
        login_response = login(verify_code)
    except Exception as e:
        print_error_and_exit(e)
    print(f"登录返回的信息为：{login_response}")
    if "success" in login_response:
        print("登录成功！接下来请不要关闭本应用以保持连接")
    else:
        print_error_and_exit("登陆失败")

    retry_num = 3
    while True:
        time.sleep(20)
        try:
            keep_active()
            retry_num = 3
        except Exception as e:
            retry_num = retry_num - 1
            if retry_num >= 0:
                print(f"保持连接失败，剩余尝试次数：{retry_num}")
                continue
            else:
                print(
                    datetime.datetime.today().strftime("%Y-%m-%d %H:%M")
                    + " 保持连接失败"
                )
                print_error_and_exit(e)
